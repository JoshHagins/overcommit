module Overcommit::Hook::PostCommit
  # Calculates the change in blame since the last revision.
  class GitGuilt < Base
    def run
      execute(%w[git guilt HEAD~1 HEAD])
      :pass
    end
  end
end
